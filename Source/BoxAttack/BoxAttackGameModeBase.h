// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BoxAttackGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BOXATTACK_API ABoxAttackGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
