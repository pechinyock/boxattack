#include "BABoxManagerBase.h"
#include "BASpawnerBase.h"
#include "BABoxBase.h"

ABABoxManagerBase::ABABoxManagerBase()
{
	PrimaryActorTick.bCanEverTick = true;
	s_SingleInstance = this;
}

void ABABoxManagerBase::BeginPlay()
{
	Super::BeginPlay();
	if (AllTemplates.Num() <= 0) {
		UE_LOG(LogTemp, Warning, TEXT("I have no templates!"));
	}
}

void ABABoxManagerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
ABABoxManagerBase* ABABoxManagerBase::s_GetBoxManager() {
	return s_SingleInstance;
}

void ABABoxManagerBase::s_CalculateDestroyPath(ABABoxBase& start, TArray<ABABoxBase*>& hisNeighbours)
{
	/*Promote '30' to const varibale*/
	/*Add some templates and check for count elemets*/
	TArray<ABABoxBase*> fullChain;
	fullChain.Init(nullptr, 30);
	fullChain[0] = &start;
	int addToindex = 1;
	int theSum = 0;
	for (size_t i = 0; i < 40; i++)
	{
		if (fullChain[i] == nullptr) {
			UE_LOG(LogTemp, Warning, TEXT("Has no start"));
			break;
		}
		TArray<ABABoxBase*> neighboursOfCurrent = fullChain[i]->GetNeighboursRef();		
		for (size_t j = 0; j < neighboursOfCurrent.Num(); j++)
		{
			if (fullChain.Contains(neighboursOfCurrent[j])) {
				continue;
			}
			if (j % 2 == 0) {
				theSum += 4;
			}
			else {
				theSum += 6;
			}			
			fullChain[addToindex] = neighboursOfCurrent[j];
			addToindex++;			
		}
	}
	int count = 0;
	for (auto& s : fullChain) {
		if (s != nullptr) {
			count++;
		}
	}	
	for (auto& t : s_SingleInstance->AllTemplates) {
		if (t.ElementsCount != count) {			
			continue;
		}
		for (auto& templ : t.Templates)
		{
			if (templ == theSum) {
				UE_LOG(LogTemp, Warning, TEXT("I FOUND IT! ELEM COUNT: %d THE SUM %d"), t.ElementsCount, templ);
				for (auto& box: fullChain)
				{
					if (box == nullptr) {
						return;
					}
					box->OnDestroy();
				}
			}
		}
	}
}

void ABABoxManagerBase::OnBoxSpawned()
{
	ExistingBoxesCount++;
}

