#include "BABoxBase.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "DrawDebugHelpers.h"
#include "BABoxManagerBase.h"
#include "BAPlayFieldBase.h"

ABABoxBase::ABABoxBase()
{
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static mesh"));
	SetRootComponent(Mesh);
	Mesh->SetSimulatePhysics(true);
	Mesh->BodyInstance.bNotifyRigidBodyCollision = true;

	XPositiveBoxChecker = CreateDefaultSubobject<UBoxComponent>(TEXT("X positive checker"));
	XNegativeBoxChecker = CreateDefaultSubobject<UBoxComponent>(TEXT("X negative checker"));

	YPositiveBoxChecker = CreateDefaultSubobject<UBoxComponent>(TEXT("Y positive checker"));
	YNegativeBoxChecker = CreateDefaultSubobject<UBoxComponent>(TEXT("Y negative checker"));

	XPositiveBoxChecker->SetupAttachment(GetRootComponent());
	XNegativeBoxChecker->SetupAttachment(GetRootComponent());

	YPositiveBoxChecker->SetupAttachment(GetRootComponent());
	YNegativeBoxChecker->SetupAttachment(GetRootComponent());

	Neighbours.Init(nullptr, 4);
}

void ABABoxBase::BeginPlay()
{
	Super::BeginPlay();
	Mesh->OnComponentHit.AddDynamic(this, &ABABoxBase::OnColliderHit);

	XPositiveBoxChecker->OnComponentBeginOverlap.AddDynamic(this, &ABABoxBase::XPositiveOverlapBegin);
	XPositiveBoxChecker->OnComponentEndOverlap.AddDynamic(this, &ABABoxBase::XPositiveOverlapEnd);

	XNegativeBoxChecker->OnComponentBeginOverlap.AddDynamic(this, &ABABoxBase::XNegativeOverlapBegin);
	XNegativeBoxChecker->OnComponentEndOverlap.AddDynamic(this, &ABABoxBase::XNegativeOverlapEnd);

	YPositiveBoxChecker->OnComponentBeginOverlap.AddDynamic(this, &ABABoxBase::YPositiveOverlapBegin);
	YPositiveBoxChecker->OnComponentEndOverlap.AddDynamic(this, &ABABoxBase::YPositiveOverlapEnd);

	YNegativeBoxChecker->OnComponentBeginOverlap.AddDynamic(this, &ABABoxBase::YNegativeOverlapBegin);
	YNegativeBoxChecker->OnComponentEndOverlap.AddDynamic(this, &ABABoxBase::YNegativeOverlapEnd);
}

void ABABoxBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

#pragma region Interfaces Implementation
void ABABoxBase::Move_Implementation()
{
	//GEngine->AddOnScreenDebugMessage(0, 4.f, FColor::Red, TEXT("Moving box"));
}

void ABABoxBase::StopMove_Implementation()
{
}

void ABABoxBase::PickMeUp_Implementation()
{
	Mesh->SetSimulatePhysics(false);
	Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ABABoxBase::OnDrop_Implementation(FVector direction)
{
	SetActorRotation(FRotator(0.f, 0.f, 0.f));
	Mesh->SetSimulatePhysics(true);
	Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Mesh->AddImpulse(direction * DropImpulse * Mesh->GetMass());
}

void ABABoxBase::OnDestroy_Implementation()
{	
	Destroy();
}
#pragma endregion

void ABABoxBase::OnColliderHit(UPrimitiveComponent* HitComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//Mesh->SetSimulatePhysics(false);
	//auto* box = Cast<ABABoxBase>(OtherActor); 
	//if (box != nullptr) {
	//	GEngine->AddOnScreenDebugMessage(1, 3.f, FColor::Red, TEXT("Box hit other box"));
	//}	
/*
	auto* floor = Cast<ABAPlayFieldBase>(OtherActor);
	if (floor != nullptr) {
		GEngine->AddOnScreenDebugMessage(12, 3.f, FColor::Green, TEXT("Box hit floor"));
	}
*/
}

#pragma region Checkers overlaps
/* X  begin */
void ABABoxBase::XPositiveOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	CalculateDestroyPath(0, OtherActor);	
}

void ABABoxBase::XNegativeOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	CalculateDestroyPath(2, OtherActor);
}

/* X  end */
void ABABoxBase::XPositiveOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Neighbours[0] = nullptr;
}

void ABABoxBase::XNegativeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Neighbours[2] = nullptr;
}

/* Y begin */
void ABABoxBase::YPositiveOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	CalculateDestroyPath(1, OtherActor);
}

void ABABoxBase::YNegativeOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	CalculateDestroyPath(3, OtherActor);
}
/* Y begin end */
void ABABoxBase::YPositiveOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Neighbours[1] = nullptr;
}


void ABABoxBase::YNegativeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Neighbours[3] = nullptr;
}
#pragma endregion


void ABABoxBase::CalculateDestroyPath(const int index, AActor* const OtherActor)
{
	if (AmICounting) {
		UE_LOG(LogTemp, Warning, TEXT("I am counting already"));
		return;
	}
	ABABoxBase* neighbour = Cast<ABABoxBase>(OtherActor);
	if (!IsValid(neighbour)) {
		UE_LOG(LogTemp, Warning, TEXT("Faild to cast AActor to ABABoxBase"));
		return;
	}
	Neighbours[index] = neighbour;
	//DrawDebugBox(GetWorld(), this->GetActorLocation() + FVector(0.f, 0.f, 10.f), FVector(30.f, 30.f, 30.f), FColor::Green, false, 5.f);
	ABABoxManagerBase::s_CalculateDestroyPath(*this, Neighbours);	
}
