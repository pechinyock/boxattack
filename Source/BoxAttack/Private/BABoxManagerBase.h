#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BABoxManagerBase.generated.h"

USTRUCT(BlueprintType)
struct FDestroyBoxTemplate {
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int ElementsCount = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<int> Templates;
};

class ABABoxBase;

UCLASS()
class ABABoxManagerBase : public AActor
{
	GENERATED_BODY()
	
public:	
	ABABoxManagerBase();

	virtual void Tick(float DeltaTime) override;

	static void s_CalculateDestroyPath(ABABoxBase& start, TArray<ABABoxBase*>& hisNeighbours);		

	static ABABoxManagerBase* s_GetBoxManager();

	inline static ABABoxManagerBase* s_SingleInstance = nullptr;

public:	
	UFUNCTION()
	void OnBoxSpawned();

	inline uint32_t GetExistingBoxesCount() const { return ExistingBoxesCount; }	

protected:
	virtual void BeginPlay() override;	

protected:
	uint32_t ExistingBoxesCount = 0;	

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FDestroyBoxTemplate> AllTemplates;	
};
