#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BASpawnerBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBoxSpawned);

struct SpawnRestrictions
{
	SpawnRestrictions(FVector origin, FVector boxExtent)
	: Origin(origin), BoxExtent(boxExtent) {}
	FVector Origin;
	FVector BoxExtent;
};

UCLASS()
class ABASpawnerBase : public AActor
{
	GENERATED_BODY()
	
public:	
	ABASpawnerBase();

	inline float GetSpawnRate() const { return SpawnRate; }
	inline void SetSpawnRate(float value) { SpawnRate = value; }

	inline void SetRestrictions(FVector origin, FVector boxExtent) { Restrictions = new SpawnRestrictions(origin, boxExtent); }

	UPROPERTY(BlueprintAssignable, Category = "Spawner Events")
	FOnBoxSpawned OnBoxSpawned;

protected:
	UPROPERTY(Editanywhere, meta = (AllowPrivateAccess = "true"), Category = "Spawner")
	TSubclassOf<AActor> ActorToSpawn;

	UPROPERTY(Editanywhere, meta = (AllowPrivateAccess = "true"), Category = "Spawner")
	TArray<TSubclassOf<AActor>> BoxesToSpawn;

	UPROPERTY(Editanywhere, meta = (AllowPrivateAccess = "true"), Category = "Spawner")
	float SpawnRate = 0.2f;

	UPROPERTY(Editanywhere, meta = (AllowPrivateAccess = "true"), Category = "Spawner")
	FVector StartBoxSpawnPoint = FVector(300.f, 300.f, 0.f);

	UPROPERTY(Editanywhere, meta = (AllowPrivateAccess = "true"), Category = "Spawner")
	bool AbleToSpawn = false;	

	UPROPERTY(Editanywhere, meta = (AllowPrivateAccess = "true"), Category = "Spawner")
	float OffsetBetweenSpawnPointsX = 50.f;

	UPROPERTY(Editanywhere, meta = (AllowPrivateAccess = "true"), Category = "Spawner")
	float OffsetBetweenSpawnPointsY = 50.f;

	UPROPERTY()
	TArray<FVector> SpawnLocations;

	int justIndex = 0;

	FTimerHandle TimerHandle;
	SpawnRestrictions* Restrictions;	

	float WeightForDefaultBox = 100.f;

protected:
	virtual void BeginPlay() override;	
	FVector GetSpawnLocation();
	
	UFUNCTION()
	TArray<FVector> InitSpawnLocations(int x, int y);

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void Spawn();
};
