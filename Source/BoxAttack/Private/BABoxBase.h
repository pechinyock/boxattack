#pragma once

#include "CoreMinimal.h"
#include "Interfaces/Movable.h"
#include "Interfaces/Pickupable.h"
#include "GameFramework/Actor.h"
#include "BABoxBase.generated.h"

UENUM()
enum EBoxType
{
	None	=	0,			/* None */
	Woden   =	1 << 0,	    /* Woden Box */
	Special =	1 << 1,	    /* Special Box */
};

UCLASS()
class ABABoxBase : public AActor, public IMovable, public IPickupable
{
	GENERATED_BODY()
	
public:	
	ABABoxBase();

	virtual void Tick(float DeltaTime) override;

public:		
#pragma region Interfaces Implementation
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BA Interfaces")
	void Move();
	virtual void Move_Implementation() override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BA Interfaces")
	void StopMove();
	virtual void StopMove_Implementation() override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BA Interfaces")
	void PickMeUp();
	virtual void PickMeUp_Implementation() override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BA Interfaces")
	void OnDrop(FVector direction);
	virtual void OnDrop_Implementation(FVector direction);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BA Interfaces")
	void OnDestroy();

	inline ABABoxBase* GetMySelf() { return this; }

	inline TArray<ABABoxBase*> GetNeighboursRef() { return Neighbours; }

	inline bool& IsItCountingAlready() { return AmICounting; }

	inline void SetIsCounting(bool value) { AmICounting = value; }		

	inline TArray<ABABoxBase*> GetValidNeighbours() const { 
		TArray<ABABoxBase*> validBoxes;
		for (auto n : Neighbours) {
			if (n != nullptr) {
				validBoxes.Add(n);
			}
		}
		return validBoxes;
	}
#pragma endregion

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Box properties")
	class UStaticMeshComponent* Mesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Box properties")
	float DropImpulse = 50.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Box properties")
	class UBoxComponent* XPositiveBoxChecker;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Box properties")
	class UBoxComponent* XNegativeBoxChecker;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Box properties")
	class UBoxComponent* YPositiveBoxChecker;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Box properties")
	class UBoxComponent* YNegativeBoxChecker;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Box properties")
	int BoxType = 1;

	UPROPERTY()
	bool AmICounting = false;

	UPROPERTY()
	TArray<ABABoxBase*> Neighbours;

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnColliderHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
						   FVector NormalImpulse, const FHitResult& Hit);	

	UFUNCTION()
	void CalculateDestroyPath(const int index, AActor* const OtherActor);	

#pragma region Checkers overlaps
	/* X */
	UFUNCTION()
	void XPositiveOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void XPositiveOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void XNegativeOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void XNegativeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	/* Y */
	UFUNCTION()
	void YPositiveOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void YPositiveOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void YNegativeOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void YNegativeOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

#pragma endregion
private:	
	inline bool IsNeighbourHasMyType(EBoxType type) const { return BoxType & type; }
};
