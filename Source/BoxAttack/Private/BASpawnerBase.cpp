#include "BASpawnerBase.h"
#include "GameFramework/Actor.h"
#include "Kismet/KismetMathLibrary.h"

#pragma region Constructor
ABASpawnerBase::ABASpawnerBase()
{
	PrimaryActorTick.bCanEverTick = true;	
}
#pragma endregion

#pragma region UE overrides
void ABASpawnerBase::BeginPlay()
{
	Super::BeginPlay();
	if (ActorToSpawn == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("Spawner: ActorToSpawn is not initialized!"));
		return;
	}
	SpawnLocations = InitSpawnLocations(8, 8);	
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ABASpawnerBase::Spawn, GetSpawnRate(), true);
}

void ABASpawnerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
#pragma endregion

#pragma region Spawn logic
void ABASpawnerBase::Spawn()
{
	if (SpawnLocations.Num() == 0) {
		UE_LOG(LogTemp, Warning, TEXT("Can not spawn anything the count of spawn locations is 0"));
		return;
	}
	if (!AbleToSpawn) {
		return;
	}
	if (Restrictions == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("Spawner: restrictions is not initialized!"));
		return;
	}
	const FVector location = UKismetMathLibrary::RandomPointInBoundingBox(Restrictions->Origin, Restrictions->BoxExtent);
	const FRotator rotation = GetActorRotation();
	if (UKismetMathLibrary::RandomBoolWithWeight(WeightForDefaultBox))
	{
		uint32_t randIndex = (uint32_t)FMath::RandRange(0, BoxesToSpawn.Num() - 1);
		uint32_t randLocIndex = (uint32_t)FMath::RandRange(0, SpawnLocations.Num() - 1);		
		GetWorld()->SpawnActor<AActor>(BoxesToSpawn[randIndex], SpawnLocations[justIndex], rotation);
	}
	if (justIndex < 64) {
		justIndex++;
	}
	OnBoxSpawned.Broadcast();
}

FVector ABASpawnerBase::GetSpawnLocation()
{
	return FVector();
}

TArray<FVector> ABASpawnerBase::InitSpawnLocations(int x, int y)
{
	int size = x * y;
	if (size <= 0) {
		UE_LOG(LogTemp, Warning, TEXT("Size can't be 0!"));
		return TArray<FVector>();
	}		
	TArray<FVector> temp;	
	for (int i = 0, j = 0; j <= y; i++) {
		if (i % y == 0) {			
			i = 0;
			j++;
		}
		temp.Push(FVector(i * OffsetBetweenSpawnPointsX, j * OffsetBetweenSpawnPointsY, GetActorLocation().Z) - StartBoxSpawnPoint);
	}		
	return temp;
}
#pragma endregion