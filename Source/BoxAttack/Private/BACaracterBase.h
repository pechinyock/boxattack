#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"
#include "BACaracterBase.generated.h"

UCLASS()
class ABACaracterBase : public ACharacter
{
	GENERATED_BODY()

public:
	ABACaracterBase();

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	inline class IPickupable* GetTarget() const { return m_Target; }
	inline void SetTarget(IPickupable* const target) { m_Target = target; }	

	inline AActor* GetUpperHeadBox() const { return m_BoxUpperHead; }
	inline void SetUpperHeadBox(AActor* const box) { m_BoxUpperHead = box; }

protected:
	virtual void BeginPlay() override;
	virtual void PawnClientRestart() override;

	void EnhancedMove(const FInputActionValue& value);
	void EnhancedRotateCamera(const FInputActionValue& value);
	void EnhancedJump(const FInputActionValue& value);
	void EnhancedPickUp(const FInputActionValue& value);

	void CameraTick(float DeltaTime);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BA Action")
	void OnBoxPickup();
	virtual void OnBoxPickup_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BA Action")
	void OnDropBox();
	virtual void OnDropBox_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BA Action")
	void OnEndBoxMoving();
	virtual void OnEndBoxMoving_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BA Action")
	void OnStartBoxMoving();
	virtual void OnStartBoxMoving_Implementation();

	UFUNCTION()
	void OnColliderHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
						   FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void OnBoxCheckerOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit);

	UFUNCTION()
	void OnBoxCheckerEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex);	

protected:
#pragma region Box logic
	class IPickupable* m_Target;
	AActor* m_BoxUpperHead;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Box checker")
	class UBoxComponent* BoxChecker;
#pragma endregion
#pragma region Camera

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera component")
	class UCameraComponent* CharacterCamera;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera component")
	TArray<FVector> CameraLocations;

	int32 CurrentCameraLocation = 0;
#pragma endregion
#pragma region EnhancedInput

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Enhanced input")
	class UInputAction* MovementAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Enhanced input")
	class UInputAction* RotateCameraAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Enhanced input")
	class UInputAction* JumpAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Enhanced input")
	class UInputAction* PickUpAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Enhanced input")
	class UInputMappingContext* MainContext;

#pragma endregion
};
