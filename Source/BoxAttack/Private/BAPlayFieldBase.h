#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BAPlayFieldBase.generated.h"

UCLASS()
class ABAPlayFieldBase : public AActor
{
	GENERATED_BODY()
	
public:	
	ABAPlayFieldBase();

	inline AActor* GetSpawner() const { return PSpawner; }	
	inline AActor* GetBoxManager() const { return PBoxManager; }

protected:
	virtual void BeginPlay() override;

protected:

	class AActor* PSpawner;
	class AActor* PBoxManager;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Play field")
	class UStaticMeshComponent* Floor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Play field")
	TSubclassOf<class ABASpawnerBase> Spawner;	

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Play field")
	FVector SpawnerOffset;		

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Play field")
	TSubclassOf<class ABABoxManagerBase> BoxManager;

public:	
	virtual void Tick(float DeltaTime) override;

private:
	void InitializeSpawner();
	void InitializeBoxManager();

};
