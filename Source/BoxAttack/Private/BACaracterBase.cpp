#include "BACaracterBase.h"
#include "Camera/CameraComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInput/Public/InputMappingContext.h"
#include "EnhancedInput/Public/EnhancedInputSubsystems.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Engine/Engine.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/CapsuleComponent.h"
#include "Components/BoxComponent.h"
#include "Interfaces/Movable.h"
#include "Interfaces/Pickupable.h"
#include "BABoxBase.h"

ABACaracterBase::ABACaracterBase()
{
	PrimaryActorTick.bCanEverTick = true;
	CharacterCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));		
	GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &ABACaracterBase::OnColliderHit);
	BoxChecker = CreateDefaultSubobject<UBoxComponent>(TEXT("Box checker"));
	BoxChecker->SetupAttachment(RootComponent);

	BoxChecker->SetGenerateOverlapEvents(true);
	BoxChecker->SetBoxExtent(FVector(100.f, 100.f, 100.f), false);
	BoxChecker->SetCollisionProfileName(TEXT("Trigger"), false);

	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->bUseControllerDesiredRotation = false;
	
	auto* animInstance = GetMesh()->GetAnimInstance();
}

#pragma region UnrealOverrides
void ABACaracterBase::BeginPlay()
{
	Super::BeginPlay();
	BoxChecker->OnComponentBeginOverlap.AddDynamic(this, &ABACaracterBase::OnBoxCheckerOverlap);
	BoxChecker->OnComponentEndOverlap.AddDynamic(this, &ABACaracterBase::OnBoxCheckerEndOverlap);
}

void ABACaracterBase::PawnClientRestart()
{
	Super::PawnClientRestart();
	if(auto PC = Cast<APlayerController>(GetController()))
	{
		if(auto enhSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PC->GetLocalPlayer()))
		{
			enhSubsystem->ClearAllMappings();
			enhSubsystem->AddMappingContext(MainContext, 0);
		}
	}
}

void ABACaracterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CameraTick(DeltaTime);
}

void ABACaracterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);

	if(UEnhancedInputComponent* PlayerEnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{

		if(JumpAction)
		{
			PlayerEnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ABACaracterBase::EnhancedJump);
		}

		if(MovementAction)
		{
			PlayerEnhancedInputComponent->BindAction(MovementAction, ETriggerEvent::Triggered, this, &ABACaracterBase::EnhancedMove);
		}

		if(RotateCameraAction)
		{
			PlayerEnhancedInputComponent->BindAction(RotateCameraAction, ETriggerEvent::Started, this, &ABACaracterBase::EnhancedRotateCamera);
		}

		if (PickUpAction) 
		{
			PlayerEnhancedInputComponent->BindAction(PickUpAction, ETriggerEvent::Started, this, &ABACaracterBase::EnhancedPickUp);
		}
	}
}

#pragma endregion

#pragma region EnhancedInputFunctions

void ABACaracterBase::EnhancedMove(const FInputActionValue& value)
{
	if(value.GetMagnitude() != .0f)
	{
		AddMovementInput(CharacterCamera->GetForwardVector(), value[1]);
		AddMovementInput(CharacterCamera->GetRightVector(), value[0]);
	}
}

void ABACaracterBase::EnhancedRotateCamera(const FInputActionValue& value)
{
	if(value.Get<float>() > .0f)
	{
		CurrentCameraLocation == 0 ? CurrentCameraLocation = CameraLocations.Num() - 1 : CurrentCameraLocation--;
	}
	if(value.Get<float>() < .0f)
	{
		CurrentCameraLocation == CameraLocations.Num() - 1 ? CurrentCameraLocation = 0 : CurrentCameraLocation++;
	}
}

void ABACaracterBase::EnhancedJump(const FInputActionValue& value)
{
	Jump();
}

void ABACaracterBase::EnhancedPickUp(const FInputActionValue& value)
{	
	if (m_BoxUpperHead != nullptr) {		
		OnDropBox();
		return;
	}
	if (!m_Target) {
		return;
	}		
	auto* targetActor = Cast<AActor>(m_Target);	
	auto* pickupable = Cast<IPickupable>(targetActor);
	pickupable->Execute_PickMeUp(targetActor);	
	targetActor->GetRootComponent()->AttachToComponent(GetMesh(),
		FAttachmentTransformRules::SnapToTargetNotIncludingScale,
		FName("UpperHead"));
	m_BoxUpperHead = targetActor;	
	this->OnBoxPickup();
}
#pragma endregion

#pragma region CameraComponent
void ABACaracterBase::CameraTick(float DeltaTime)
{
	if(CameraLocations.IsEmpty())
	{
		return;
	}
	CharacterCamera->SetRelativeLocation(FMath::VInterpTo(CharacterCamera->GetRelativeLocation(), CameraLocations[CurrentCameraLocation], DeltaTime, 3.f));
	float lookToCenter = UKismetMathLibrary::FindLookAtRotation(CharacterCamera->GetRelativeLocation(), FVector(0.f, 0.f, 0.f)).Yaw;
	CharacterCamera->SetRelativeRotation(FRotator(-30.f, lookToCenter, 0.f));
}
#pragma endregion

#pragma region interfaces impls
void ABACaracterBase::OnDropBox_Implementation()
{
	if (m_BoxUpperHead == nullptr) {
		return;
	}
	m_BoxUpperHead->GetRootComponent()->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	auto baBox = Cast<ABABoxBase>(m_BoxUpperHead);
	if (baBox == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("WTF ARE Carrying"));
		return;
	}
	baBox->OnDrop(GetActorForwardVector());
	m_BoxUpperHead = nullptr;
}

void ABACaracterBase::OnBoxPickup_Implementation()
{	

}

void ABACaracterBase::OnEndBoxMoving_Implementation()
{

}

void ABACaracterBase::OnStartBoxMoving_Implementation()
{	
	//GEngine->AddOnScreenDebugMessage(0, 4.f, FColor::Yellow, TEXT("Moving box"));
}
#pragma endregion

#pragma region Hits/Overlaps
void ABACaracterBase::OnColliderHit(UPrimitiveComponent* HitComp, AActor* OtherActor,
									UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{	

}

void ABACaracterBase::OnBoxCheckerOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{	
	IMovable* objToMove = Cast<IMovable>(OtherActor);
	if (objToMove)
	{
		auto box = Cast<IPickupable>(OtherActor);
		if (box) {
			SetTarget(box);
		}
		objToMove->Execute_Move(OtherActor);
		this->OnStartBoxMoving();
	}	
}

void ABACaracterBase::OnBoxCheckerEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{	
	IMovable* objToMove = Cast<IMovable>(OtherActor);
	if (objToMove)
	{		
		m_Target = nullptr;
		this->OnEndBoxMoving();
		objToMove->Execute_StopMove(OtherActor);
	}
}
#pragma endregion 