#include "BAPlayFieldBase.h"
#include "BASpawnerBase.h"
#include "Components/StaticMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "BABoxManagerBase.h"
#include "BASpawnerBase.h"

ABAPlayFieldBase::ABAPlayFieldBase()
{
	PrimaryActorTick.bCanEverTick = true;
	Floor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Floor"));	
	SetRootComponent(Floor);
}

void ABAPlayFieldBase::BeginPlay()
{
	Super::BeginPlay();
	InitializeSpawner();
	InitializeBoxManager();
}

void ABAPlayFieldBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABAPlayFieldBase::InitializeSpawner()
{
	if (Spawner == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("PlayField: Spawner is not initialized!"));
	}
	const FVector location = this->GetActorLocation() + SpawnerOffset;
	const FRotator rotation = this->GetActorRotation();
	PSpawner = GetWorld()->SpawnActor<AActor>(Spawner, location, rotation);
	auto spawner = Cast<ABASpawnerBase>(PSpawner);
	if (spawner == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("PlayField: Couldn't cast spawner!"));
		return;
	}
	FVector origin;
	FVector boxExtent;	
	this->GetActorBounds(false, origin, boxExtent);	
	spawner->SetRestrictions(origin, boxExtent - FVector(80.f, 80.f, 0.f));
}

void ABAPlayFieldBase::InitializeBoxManager()
{
	if (BoxManager == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("PlayField: BoxManager is not initialized!"));
	}
	PBoxManager = GetWorld()->SpawnActor<AActor>(BoxManager, this->GetActorLocation(), this->GetActorRotation());
	auto boxManager = Cast<ABABoxManagerBase>(PBoxManager);
	if (boxManager == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("PlayField: Can't cast AActor to ABABoxmanager"));
		return;
	}
	auto spawner = Cast<ABASpawnerBase>(PSpawner);
	if (spawner == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("PlayField: Can't cast AActor to ABASpawnerBase"));
		return;
	}
	spawner->OnBoxSpawned.AddDynamic(boxManager, &ABABoxManagerBase::OnBoxSpawned);
}
